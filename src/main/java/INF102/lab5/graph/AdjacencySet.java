package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private final Map<V, Set<V>> _graph = new HashMap<>();

    @Override
    public int size() {
        return _graph.size();
    }

    @Override
    public void addNode(V node) {
        _graph.putIfAbsent(node, new HashSet<V>());
    }

    @Override
    public void removeNode(V node) {
        _graph.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        addNode(u);
        addNode(v);
        _graph.get(u).add(v);
        _graph.get(v).add(u);
    }

    @Override
    public void removeEdge(V u, V v) {
        var uSet = _graph.get(u);
        if (uSet != null) uSet.remove(v);

        var vSet = _graph.get(v);
        if (vSet != null) vSet.remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return _graph.containsKey(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        var uSet = _graph.get(u);
        if (uSet == null) return false;
        return uSet.contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        var set = _graph.get(node);
        if (set == null) return new HashSet<V>();
        return new HashSet<V>(set);
    }

    @Override
    public boolean connected(V u, V v) {
        return findNode(u, v, new HashSet<V>(size()));
    }

    private boolean findNode(V root, V target, Set<V> checked) {
        if (checked.contains(root)) 
            return false;

        // Funny bug where "==" only works for integers in range -128 to 127.
        // Caused code to fail for values greater than 127.
        // if (root == target)
        //     return true;

        if (target.equals(root))
            return true;

        checked.add(root);

        for (var child : getNeighbourhood(root))
            if (findNode(child, target, checked))
                return true;

        return false;
    }
}
